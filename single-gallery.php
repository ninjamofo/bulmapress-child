<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Bulmapress
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main wrapper" role="main">
		<?php while ( have_posts() ) : the_post(); ?>



<section class="section">
<div class="container">
<div class="columns is-multiline">
	
	<?php $attachments = get_attached_media( 'image' ) ; foreach ($attachments as $attachment) { ?>
	<div class="column is-4">
	<div class="box box-model is-paddingless">
	<a class="carousal" href="<?php echo wp_get_attachment_image_src($attachment -> ID, 'full')[0]; ?>"><img src="<?php $img = wp_get_attachment_image_src( $attachment -> ID, 'medium' ); echo $img[0];?>"  alt="<?php echo $attachment -> post_title; ?>" /></a>
	</div>
	</div>
	<?php } ?>
	</div>
	</div>
			<div class="section">
				<div class="container is-narrow">
					<?php the_post_navigation();?>
				</div>
			</div>
			<?php bulmapress_get_comments(); ?>
		<?php endwhile; ?>
	</main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
